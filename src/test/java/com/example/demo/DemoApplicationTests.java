package com.example.demo;

import java.util.function.Supplier;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

	@Test
	void contextLoads() {
	}


    @Test
    public void test() {
        Supplier<String> oldSuppiler = new Supplier() {
            @Override
            public String get() {
                return "h";
            }
        };

        Supplier<String> supplier = () -> "h";
        System.out.println(oldSuppiler.get());
        System.out.println(supplier.get());
    }

}
